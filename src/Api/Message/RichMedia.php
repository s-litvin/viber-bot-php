<?php

namespace Viber\Api\Message;

use Viber\Api\Message;

/**
 * Rich Media as message
 *
 * @author Litvin Sergey <litvin.xx@gmail.com>
 */
class RichMedia extends Message
{
    /**
     * Rich Media array
     *
     * @var array
     */
    protected $media = [
        "Type" => "rich_media",
        "Buttons" => [
            [
                "Columns" => 6,
                "Rows" => 3,
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "Image" => "http://html-test:8080/myweb/guy/assets/imageRMsmall2.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 2,
                "Text" => "<font color=#323232><b>Headphones with Microphone, On-ear Wired earphones</b></font><font color=#777777><br>Sound Intone </font><font color=#6fc133>$17.99</font>",
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "TextSize" => "medium",
                "TextVAlign" => "middle",
                "TextHAlign" => "left"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#ffffff>Buy</font>",
                "TextSize" => "large",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle",
                "Image" => "https://s14.postimg.org/4mmt4rw1t/Button.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#8367db>MORE DETAILS</font>",
                "TextSize" => "small",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle"
            ],
            [
                "Columns" => 6,
                "Rows" => 3,
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "Image" => "https://s16.postimg.org/wi8jx20wl/image_RMsmall2.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 2,
                "Text" => "<font color=#323232><b>Hanes Men's Humor Graphic T-Shirt</b></font><font color=#777777><br>Hanes</font><font color=#6fc133>$10.99</font>",
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "TextSize" => "medium",
                "TextVAlign" => "middle",
                "TextHAlign" => "left"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#ffffff>Buy</font>",
                "TextSize" => "large",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle",
                "Image" => "https://s14.postimg.org/4mmt4rw1t/Button.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#8367db>MORE DETAILS</font>",
                "TextSize" => "small",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle"
            ],
            [
                "Columns" => 6,
                "Rows" => 3,
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "Image" => "https://s16.postimg.org/wi8jx20wl/image_RMsmall2.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 2,
                "Text" => "<font color=#323232><b>Hanes Men's Humor Graphic T-Shirt</b></font><font color=#777777><br>Hanes</font><font color=#6fc133>$10.99</font>",
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "TextSize" => "medium",
                "TextVAlign" => "middle",
                "TextHAlign" => "left"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#ffffff>Buy</font>",
                "TextSize" => "large",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle",
                "Image" => "https://s14.postimg.org/4mmt4rw1t/Button.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#8367db>MORE DETAILS</font>",
                "TextSize" => "small",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle"
            ],
            [
                "Columns" => 6,
                "Rows" => 3,
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "Image" => "https://s16.postimg.org/wi8jx20wl/image_RMsmall2.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 2,
                "Text" => "<font color=#323232><b>Hanes Men's Humor Graphic T-Shirt</b></font><font color=#777777><br>Hanes</font><font color=#6fc133>$10.99</font>",
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "TextSize" => "medium",
                "TextVAlign" => "middle",
                "TextHAlign" => "left"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#ffffff>Buy</font>",
                "TextSize" => "large",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle",
                "Image" => "https://s14.postimg.org/4mmt4rw1t/Button.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#8367db>MORE DETAILS</font>",
                "TextSize" => "small",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle"
            ],
            [
                "Columns" => 6,
                "Rows" => 3,
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "Image" => "https://s16.postimg.org/wi8jx20wl/image_RMsmall2.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 2,
                "Text" => "<font color=#323232><b>Hanes Men's Humor Graphic T-Shirt</b></font><font color=#777777><br>Hanes</font><font color=#6fc133>$10.99</font>",
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "TextSize" => "medium",
                "TextVAlign" => "middle",
                "TextHAlign" => "left"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#ffffff>Buy</font>",
                "TextSize" => "large",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle",
                "Image" => "https://s14.postimg.org/4mmt4rw1t/Button.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#8367db>MORE DETAILS</font>",
                "TextSize" => "small",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle"
            ],
            [
                "Columns" => 6,
                "Rows" => 3,
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "Image" => "https://s16.postimg.org/wi8jx20wl/image_RMsmall2.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 2,
                "Text" => "<font color=#323232><b>Hanes Men's Humor Graphic T-Shirt</b></font><font color=#777777><br>Hanes</font><font color=#6fc133>$10.99</font>",
                "ActionType" => "open-url",
                "ActionBody" => "https://www.google.com",
                "TextSize" => "medium",
                "TextVAlign" => "middle",
                "TextHAlign" => "left"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#ffffff>Buy</font>",
                "TextSize" => "large",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle",
                "Image" => "https://s14.postimg.org/4mmt4rw1t/Button.png"
            ],
            [
                "Columns" => 6,
                "Rows" => 1,
                "ActionType" => "reply",
                "ActionBody" => "https://www.google.com",
                "Text" => "<font color=#8367db>MORE DETAILS</font>",
                "TextSize" => "small",
                "TextVAlign" => "middle",
                "TextHAlign" => "middle"
            ]
        ]];

    protected $cols = 6;
    protected $rows = 7;
    protected $bgColor = 'FFFFFF';
    protected $buttons = [];
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return Type::RICHMEDIA;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
            'rich_media' => $this->getRichMedia()
        ]);
    }

    public function setRichMedia($buttons, $columns='6', $rows='7', $bgColor='FFFFFF')
    {
        $this->columns = ((int)$columns <= 6 && (int)$columns>=1) ? $columns : 6;
        $this->rows = ((int)$rows <= 7 && (int)$rows>=1) ? $rows : 7;
        $this->bgColor = $bgColor;
        $this->buttons = is_array($buttons) ? $buttons : [];
        return $this;
    }

    private function getRichMedia()
    {
        $this->media['ButtonsGroupColumns'] = $this->cols;
        $this->media['ButtonsGroupRows'] = $this->rows;
        $this->media['BgColor'] = '#' . trim($this->bgColor, '#');
        \Log::debug('BUTTONS');
        \Log::debug($this->buttons);
        $this->media['Buttons'] = $this->buttons ? $this->buttons : $this->media['Buttons'];

        return $this->media;
    }
}
